/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoDeDados;

import model.Estoque;
import model.ItemEstoque;

/**
 *
 * @author gt4w
 */
public class EstoqueDAO {
    
    public boolean adiciona(ItemEstoque item) throws ModelException{
        ItemEstoque novo = null;
	String sql = "SELECT * FROM estoque WHERE id_produto = ? ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
	
	db.setInt(1,item.getProduto().getId());
		
	db.executeQuery();
	
	if(db.next()){
            
            novo = create(db);
		
	}
        
        if(novo == null){
            
            db.prepareStatement("insert into estoque values (?,?) ; ");
		
            db.setInt(1,item.getProduto().getId());
            db.setDouble(2,item.getQuantidade());
            db.executeUpdate();
            
        }else{
            
            db.prepareStatement("update estoque set quantidade = ? where id_produto = ? ; ");
            
            db.setDouble(1,item.getQuantidade() + novo.getQuantidade());
            db.setInt(2,item.getProduto().getId());
            db.executeUpdate();
        }
        
        return true;
    }
    
    
    public Estoque getEstoque() throws ModelException{
        ItemEstoque novo = null;
        Estoque estoque = new Estoque();
	String sql = "SELECT * FROM estoque ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);

		
	db.executeQuery();
	
	while(db.next()){
            
           estoque.getEstoque().add(create(db));
         
	}
        
        return estoque;
    }
    
    
    public boolean baixa(ItemEstoque item) throws ModelException{
        ItemEstoque novo = null;
	String sql = "SELECT * FROM estoque WHERE id_produto = ? ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
	
	db.setInt(1,item.getProduto().getId());
		
	db.executeQuery();
	
	if(db.next()){
            
            novo = create(db);
            db.prepareStatement("update estoque set quantidade = ? where id_produto = ? ; ");
            
            db.setDouble(1,novo.getQuantidade() - item.getQuantidade());
            db.setInt(2,item.getProduto().getId());
		
	}
        
        return true;
    }
    
    
    private ItemEstoque create(DBHandler db) throws ModelException{
        ProdutoDAO p = new ProdutoDAO();
        ItemEstoque item = new ItemEstoque();
        
        item.setProduto(p.findById(db.getInt("id_produto")));
        item.setQuantidade(db.getDouble("quantidade"));
        
        return item;
    }
    
}
