package bancoDeDados;


import model.Usuario;

public class UsuarioDAO {
	
public boolean validaUsuario(Usuario u) throws ModelException{
		
	String sql = "SELECT * FROM usuario WHERE login = ? ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
	
	db.setString(1,u.getLogin());
		
	db.executeQuery();
	
	if(db.next()){
		
		if(u.getSenha().equals(db.getString("senha"))){
			u.setId(db.getInt("id"));
			db.close();
			return true;
		}
		
	}
	db.close();
		return false;
		
	}

        public boolean save(Usuario u) throws ModelException {
			
		DBHandler db = new DBHandler();
		
		db.prepareStatement("insert into usuario values (default,?,?) ; ");
		
                db.setString(1, u.getLogin());
		db.setString(2,u.getSenha());
                
	return	db.executeUpdate() > 0;
	
	}

}
