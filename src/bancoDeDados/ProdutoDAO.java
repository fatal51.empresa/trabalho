/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoDeDados;

import java.util.ArrayList;
import java.util.List;
import model.Produto;

/**
 *
 * @author gt4w
 */
public class ProdutoDAO {
    
    public Produto findById(Integer id) throws ModelException{
		
	String sql = "SELECT * FROM produto WHERE id = ? ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
	
	db.setInt(1,id);
		
	db.executeQuery();
	
	if(db.next()){
            
            return create(db);
	
	}
	db.close();
		
        return null;
		
    }
    
    public Produto findByNome(String nome) throws ModelException{
		
	String sql = "SELECT * FROM produto WHERE nome = ? ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
	
	db.setString(1,nome);
		
	db.executeQuery();
	
	if(db.next()){
            
            return create(db);
	
	}
	db.close();
		
        return null;
		
    }

        public boolean save(Produto produto) throws ModelException {
			
            DBHandler db = new DBHandler();

            db.prepareStatement("insert into produto values (default,?,?) ; ");

            db.setString(1, produto.getNome());
            db.setDouble(2,produto.getValor());

            return db.executeUpdate() > 0;
	
	}

        public List<Produto> getProdutos() throws ModelException{
            
        List<Produto> produtos = new ArrayList();
	String sql = "SELECT * FROM produto ; ";
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement(sql);
		
	db.executeQuery();
	
	while(db.next()){
            produtos.add(create(db));
	}
	db.close();
		
        return produtos;
            
        }
        
        
    private Produto create(DBHandler db) throws ModelException{
       return new Produto(db.getInt("id"), db.getDouble("valor"), db.getString("nome")); 
    }
    
}
