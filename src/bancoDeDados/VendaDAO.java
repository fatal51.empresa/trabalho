/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bancoDeDados;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Venda;

/**
 *
 * @author gt4w
 */
public class VendaDAO {
    
    public boolean adiciona(Venda venda) throws ModelException{
	
	DBHandler db = new DBHandler();
	
	db.prepareStatement("insert into venda values (default,?,?) ; ");
	
	db.setDate(1,new Date());
        db.setDouble(2,venda.getValor());
		
	db.executeUpdate();
        
        return true;
    }
    
    
    public List<Venda> list() throws ModelException{
	
        List<Venda> vendas = new ArrayList();
                
	DBHandler db = new DBHandler();
	
	db.prepareStatement("select * from venda ; ");
		
	db.executeQuery();
        
        while(db.next()){
            vendas.add(create(db));
        }
        
        return vendas;
    }
    
    
    private Venda create(DBHandler db) throws ModelException{
        Venda venda = new Venda();
        
        venda.setData(db.getDate("data"));
        venda.setValor(db.getDouble("valor"));
        venda.setId(db.getInt("id"));
        System.out.println(db.getInt("id"));
        return venda;
    }
    
}
