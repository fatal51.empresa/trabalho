/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  gt4w
 * Created: 07/12/2019
 */


create database trabalho;

use trabalho;

create table usuario(
    id int not null auto_increment,
    login varchar(30) not null,
    senha varchar(100) not null,
    
    primary key(id)
);


create table produto(
    id int not null auto_increment,
    nome varchar(30) not null,
    valor decimal(5,2) not null,
    
    primary key(id)
);


create table estoque(
    id_produto int not null,
    quantidade decimal(5,2) not null,
    
    primary key(id_produto),
    foreign key(id_produto) references produto(id)
);


create table venda(
    id int not null auto_increment,
    data date not null,
    valor decimal(5,2),
	
    primary key(id)
);



