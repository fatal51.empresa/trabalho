/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bancoDeDados.ModelException;
import bancoDeDados.VendaDAO;

/**
 *
 * @author gt4w
 */
public class Venda {
    
    private List<ItemEstoque> produtos;
    
    public Venda(){
        produtos = new ArrayList();
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    private Date data;
    private Double valor;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ItemEstoque> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ItemEstoque> produtos) {
        this.produtos = produtos;
    }
    
    public void add(ItemEstoque item) {
        produtos.add(item);
    }
    
    public boolean finalizaVenda(){
        
        try {
            new VendaDAO().adiciona(this);
        } catch (ModelException ex) {
            Logger.getLogger(Venda.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public Double getValor() {
        
        Double valor = 0.0;
        
        if(produtos.size() == 0)
        	return this.valor;
        
        for(ItemEstoque produto : produtos){
            valor += produto.getQuantidade() * produto.getProduto().getValor();
        }
        
        return valor;
    }
    
}
